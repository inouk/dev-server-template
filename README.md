# Inouk Buildit Odoo Docker Image

This builds Odoo base Docker Images designed to be used with ikb.

## Build Images
This repository contains Dockerfiles for several images designed to be used as base images for different Odoo versions:
 - inouk.odoo.base.ikb13c
 
## Description
Each images contains:
 - all system packages required to run Odoo
 - python
 - last version ok inouk buildit
 - username is muppy
 - an /opt/muppy directory where your package are suppsed to be installed.
 
You can use generated images as base images for your python postgresql projects.

These images contains no Odoo specific feature but rather they include a set of packages required by Odoo. The wkhtmltopdf toolkit is a perfect example.

## Installation
This requires only a running Docker.
Please refer to https://docs.docker.com/engine/install/ubuntu/

### muppy.io Dev Servers
Use Docker installation Task.
Don't forget to setup privileged LXC

```
  sudo lxc profile edit muppy
  # Insert security.nesting line as:

  config:
    security.nesting: "true"
    user.user-data: |-
    ...
```

**This will apply to newly created LXCs**

To apply on an existing LXC. insert the security.nesting directives in the LXC config using:

```
  sudo lxc config edit ikb-odoo-docker-dev-cyril

```

## Usages

### Usage of this image

See .devcontainer/docker-commands.sh for instructions on how to generate base images.

### Usage of images generated from this repository

Just reference the image using FROM in your docker file.

## Use generated images

Ready to use images are stored in *Gitlab Container Registry*




## Support
Support is available only for muppy.io customers.

## Contributing
Contributions are made only through Merge Requests.
We are open to contributions, but open a ticket before to check general receivability of the feature you plan to implement.

## Authors and acknowledgment
Cyril MORISSE

## License
This project is licenced under MIT Licence.

## Project status
This project is active.
