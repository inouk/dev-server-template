# 
# Shell commands to build and run Ikb Odoo Docker 
#
#
# Build 
# This must be ran in the appserver / workspace folder
docker build --pull --no-cache --rm -f ".devcontainer/Dockerfile" -t ikb-focal-base:latest . 
docker build --pull            --rm -f ".devcontainer/Dockerfile" -t ikb-focal-base:latest .   
#                                                                                          ^-context                      

# Images are stored in Gitlab Registry
# Push
# See: https://docs.gitlab.com/ee/user/packages/container_registry/index.html
docker login registry.gitlab.com  # Use a PAT
docker tag ikb-focal-base:latest registry.gitlab.com/inouk/dev-server-template/ikb-focal-base:latest
docker push registry.gitlab.com/inouk/dev-server-template/ikb-focal-base:latest
# Run
docker run -it ikb-focal-base:latest /bin/bash
docker run -it registry.gitlab.com/inouk/dev-server-template/ikb-focal-base:latest /bin/bash
