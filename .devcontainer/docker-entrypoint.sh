#!/usr/bin/env bash

# Ref: https://code.visualstudio.com/remote/advancedcontainers/start-processes
echo "Starting docker-entrypoint.sh"
echo -e "Executing: \"$@\"... "
echo -e "Remember you can \"Attach a Shell\" if you need it !"
exec "$@"